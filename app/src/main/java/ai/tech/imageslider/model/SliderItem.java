package ai.tech.imageslider.model;

public class SliderItem {
    private int image;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public SliderItem(int image) {
        this.image = image;
    }
}
