package ai.tech.imageslider.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;

import ai.tech.imageslider.R;
import ai.tech.imageslider.adapter.VideoAdapter;
import ai.tech.imageslider.model.VideoItem;

public class SwipeableVideosActivity extends AppCompatActivity {

    private ViewPager2 viewPager2;
    private ArrayList<VideoItem> videoList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipeable_videos);

        viewPager2 = findViewById(R.id.viewPager2);

        videoList.add(new VideoItem("http://www.infinityandroid.com/videos/video1.mp4",
                "Video 1", "http://www.infinityandroid.com/videos/video1.mp4"));
        videoList.add(new VideoItem("http://www.infinityandroid.com/videos/video2.mp4",
                "Video 4", "http://www.infinityandroid.com/videos/video1.mp4"));
        videoList.add(new VideoItem("http://www.infinityandroid.com/videos/video3.mp4",
                "Video 3", "http://www.infinityandroid.com/videos/video1.mp4"));

        viewPager2.setAdapter(new VideoAdapter(videoList));
    }
}