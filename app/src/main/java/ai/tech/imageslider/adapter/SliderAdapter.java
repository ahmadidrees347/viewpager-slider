package ai.tech.imageslider.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import ai.tech.imageslider.R;
import ai.tech.imageslider.model.SliderItem;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewHolder> {

    private ArrayList<SliderItem> sliderList;

    public SliderAdapter(ArrayList<SliderItem> sliderList) {
        this.sliderList = sliderList;
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SliderViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_container_slider, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {
        holder.setSliderImage(sliderList.get(position));
    }

    @Override
    public int getItemCount() {
        return sliderList.size();
    }

    public static class SliderViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView imageView;

        public SliderViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.slider_image);
        }

        public void setSliderImage(SliderItem item) {
            imageView.setImageResource(item.getImage());
        }
    }
}
